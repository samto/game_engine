#include "Transform.h"
#include <iostream>

Transform::Transform() : Component("Transform"){

    m_pos = QVector3D(0,0,0);
    m_rot = QQuaternion(0,0,0,1);
    m_scale = QVector3D(1,1,1);

    m_parentMatrix = QMatrix4x4();
    m_parentMatrix.setToIdentity();

    setParent(nullptr);
}


Transform::Transform(Transform* in_parentTransform) : Component("Transform"){

    m_pos = QVector3D(0,0,0);
    m_rot = QQuaternion(0,0,0,1);
    m_scale = QVector3D(1,1,1);

    m_parentMatrix = QMatrix4x4();
    m_parentMatrix.setToIdentity();

    setParent(in_parentTransform);
}

Transform::~Transform(){
}

//Local to Parent
QMatrix4x4 Transform::getLocalTransformation()
{
    QMatrix4x4 translationMatrix = QMatrix4x4();
    QMatrix4x4 rotationMatrix = QMatrix4x4();
    QMatrix4x4 scaleMatrix = QMatrix4x4();

    translationMatrix.translate(m_pos);
    rotationMatrix.rotate(m_rot);
    scaleMatrix.scale(m_scale);

    return (translationMatrix * (rotationMatrix * scaleMatrix));
}

//Parent to Local
QMatrix4x4 Transform::getLocalInverseTransformation()
{
    QMatrix4x4 translationMatrix = QMatrix4x4();
    QMatrix4x4 rotationMatrix = QMatrix4x4();
    QMatrix4x4 scaleMatrix = QMatrix4x4();

    QVector3D invScale = m_scale;

    invScale[0] = (m_scale[0] == 0.0 ? 0.0f : 1.0/m_scale[0]);
    invScale[1] = (m_scale[1] == 0.0 ? 0.0f : 1.0/m_scale[1]);
    invScale[2] = (m_scale[2] == 0.0 ? 0.0f : 1.0/m_scale[2]);

    translationMatrix.translate(-m_pos);
    rotationMatrix.rotate(m_rot.inverted());
    scaleMatrix.scale(invScale);

    return (scaleMatrix * (rotationMatrix * translationMatrix));
}

//Local to World
QMatrix4x4 Transform::getGlobalTransformation()
{
    if(m_parent != nullptr){
        return m_parent->getGlobalTransformation() * getLocalTransformation();
    }
    else {
        return getLocalTransformation();
    }
}

//World to local
QMatrix4x4 Transform::getGlobalInverseTransformation()
{
    if(m_parent != nullptr){
        return getLocalInverseTransformation() * m_parent->getGlobalInverseTransformation();
    }
    else {
        return getLocalInverseTransformation();
    }
}

void Transform::setParent(Transform* parent)
{
    this->m_parent = parent;
}

QVector3D Transform::applyToPoint(QVector3D in_point)
{
    QMatrix4x4 transformMat = getGlobalTransformation();
    return multiplyMatrix(transformMat,in_point) + QVector3D(transformMat(0,3),transformMat(1,3),transformMat(2,3));
}

QVector3D Transform::applyToVector(QVector3D in_vector)
{
    QMatrix4x4 transformMat = getGlobalTransformation();
    return multiplyMatrix(transformMat,in_vector);
}

QVector3D Transform::getTransformedPos()
{
    QMatrix4x4 parentMat = getGlobalTransformation();
    return multiplyMatrix(parentMat,m_pos) + QVector3D(parentMat(0,3),parentMat(1,3),parentMat(2,3));
}

QQuaternion Transform::getTransformedRot()
{
    QQuaternion parentRotation = QQuaternion(0,0,0,1);

    if(m_parent != nullptr)
        parentRotation = m_parent->getTransformedRot();

    return parentRotation * m_rot;
}

//Uniform scaling
void Transform::scale(float scale){
    this->m_scale = QVector3D(scale,scale,scale);
}

void Transform::translate(QVector3D translation){
    this->m_pos += translation;
}

void Transform::setPosition(QVector3D position){
    this->m_pos = position;
}

void Transform::rotate(QVector3D eulerAngles){
    this->m_rot = QQuaternion::fromAxisAndAngle(1.0f, 0.0f, 0.0f, this->m_rot.toEulerAngles()[0] + eulerAngles[0])
                  * QQuaternion::fromAxisAndAngle(0.0f, 1.0f, 0.0f, this->m_rot.toEulerAngles()[1] + eulerAngles[1])
                  * QQuaternion::fromAxisAndAngle(0.0f, 0.0f, 1.0f, this->m_rot.toEulerAngles()[2] + eulerAngles[2]);
}

void Transform::setRotation(QVector3D eulerAngles){
    this->m_rot = QQuaternion::fromAxisAndAngle(1.0f, 0.0f, 0.0f, eulerAngles[0])
                  * QQuaternion::fromAxisAndAngle(0.0f, 1.0f, 0.0f, eulerAngles[1])
                  * QQuaternion::fromAxisAndAngle(0.0f, 0.0f, 1.0f, eulerAngles[2]);
}

QVector3D Transform::getPosition()
{
    return m_pos;
}

QQuaternion Transform::getRotation()
{
    return m_rot;
}

QVector3D Transform::getScale()
{
    return m_scale;
}


QVector3D multiplyMatrix(QMatrix4x4 in_matrix, QVector3D in_vector){
    QVector3D out_result = QVector3D(0.0f,0.0f,0.0f);
    for(int row = 0; row < 3; row++){
        for(int column = 0; column < 3; column++){
            out_result[row] += in_matrix(row,column) * in_vector[column];
        }
    }

    return out_result;
}

