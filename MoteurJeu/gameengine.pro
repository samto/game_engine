QT += core gui widgets

TARGET = gameengine
TEMPLATE = app

SOURCES += main.cpp \
    GameObject.cpp \
    Transform.cpp \
    boundingbox.cpp \
    camera.cpp \
    component.cpp \
    lighting.cpp \
    mesh.cpp \
    mainwidget.cpp \
    tiny_obj_loader.cc

HEADERS += \
    GameObject.h \
    Transform.h \
    boundingbox.h \
    camera.h \
    component.h \
    lighting.h \
    mainwidget.h \
    mesh.h \
    tiny_obj_loader.h

RESOURCES += \
    meshes.qrc \
    shaders.qrc \
    textures.qrc

# install
target.path = /home/e20170000215/Documents/M2/Appli_Interactive/Projet_MoteurJeu/game_engine/MoteurJeu/
INSTALLS += target

DISTFILES +=

