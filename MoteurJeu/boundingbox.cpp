#include "boundingbox.h"

BoundingBox::BoundingBox() : Component("BoundingBox")
{

}

BoundingBox::BoundingBox(Mesh* in_linkedMesh, Transform* in_linkedTransform) : Component("BoundingBox")
{
    this->setLinkedComponents(in_linkedMesh, in_linkedTransform);
}

BoundingBox::~BoundingBox(){

}

void BoundingBox::setLinkedComponents(Mesh* in_linkedMesh, Transform* in_linkedTransform){
    this->linkedMesh = in_linkedMesh;
    this->linkedTransform = in_linkedTransform;

    QVector3D bbFirstPoint = in_linkedMesh->getBoundingBoxPos(0);
    QVector3D bbSecondPoint = in_linkedMesh->getBoundingBoxPos(1);

    this->startPoint = QVector3D(std::min(bbFirstPoint[0],bbSecondPoint[0]),std::min(bbFirstPoint[1],bbSecondPoint[1]),std::min(bbFirstPoint[2],bbSecondPoint[2]));
    this->endPoint = QVector3D(std::max(bbFirstPoint[0],bbSecondPoint[0]),std::max(bbFirstPoint[1],bbSecondPoint[1]),std::max(bbFirstPoint[2],bbSecondPoint[2]));

    debugBoundingBoxMesh = new Mesh();
    debugBoundingBoxMesh->setBoundingBoxCorners(this->startPoint,this->endPoint);
    debugBoundingBoxMesh->initBoundingBox();
}

bbPoint BoundingBox::getStartPoint(){
    return bbPoint(linkedTransform->applyToPoint(this->startPoint),this);
}

bbPoint BoundingBox::getEndPoint(){
    return bbPoint(linkedTransform->applyToPoint(this->endPoint),this);
}

unsigned int BoundingBox::getNbCollisions(){
    return this->inCollisionWith.size();
}
void BoundingBox::addCollision(BoundingBox* in_boundingBox){
    this->inCollisionWith.push_back(in_boundingBox);
}

void BoundingBox::removeCollision(BoundingBox* in_boundingBox){
    for(int cptCol = 0; cptCol < this->getNbCollisions(); cptCol++){
        if(this->inCollisionWith[cptCol] == in_boundingBox){
            inCollisionWith.erase(inCollisionWith.begin()+cptCol);
            return;
        }
    }
}

void BoundingBox::clearCollisions(){
    inCollisionWith.clear();
}

bool BoundingBox::collidesWith(BoundingBox* in_boundingBox){
    for(int cptCol = 0; cptCol < this->getNbCollisions(); cptCol++){
        if(this->inCollisionWith[cptCol] == in_boundingBox){
            return true;
        }
    }
    return false;
}

BoundingBox* BoundingBox::getCollisionByIndex(int index){
    return this->inCollisionWith[index];
}

void BoundingBox::drawDebugBoundingBox(QOpenGLShaderProgram *program){
    if(debugBoundingBoxMesh != nullptr){
        //Set line color (debug purpose)
        if(getNbCollisions() > 0){
            debugBoundingBoxMesh->setMaterial(red);
        } else {
            debugBoundingBoxMesh->setMaterial(green);
        }

        debugBoundingBoxMesh->draw(program);
    }
}
