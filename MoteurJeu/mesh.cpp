/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtCore module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "mesh.h"

//! [0]
Mesh::Mesh()
    : Component("Mesh"), indexBuf(QOpenGLBuffer::IndexBuffer)
{
    initializeOpenGLFunctions();

    // Generate 2 VBOs
    arrayBuf.create();
    indexBuf.create();

    this->setBoundingBoxCorners(QVector3D(0.0,0.0,0.0),QVector3D(0.0,0.0,0.0));
}

Mesh::~Mesh()
{
    arrayBuf.destroy();
    indexBuf.destroy();
}
//! [0]

void Mesh::initCubeGeometry()
{
    // For cube we would need only 8 vertices but we have to
    // duplicate vertex for each face because texture coordinate
    // is different.
    // Hence 4 vertices per face and 6 faces vertices = 24 vertices

    unsigned int vertexNumber = 24 ;
    VertexData vertices[] = {
        // Vertex data for face 0
        {QVector3D(-1.0f, -1.0f,  1.0f), QVector2D(0.0f, 0.33f), QVector3D(0.0f, 1.0f, 0.0f)}, // v0 - Top side
        {QVector3D( 1.0f, -1.0f,  1.0f), QVector2D(0.25f, 0.33f), QVector3D(0.0f, 1.0f, 0.0f)}, // v1
        {QVector3D(-1.0f,  1.0f,  1.0f), QVector2D(0.0f, 0.66f), QVector3D(0.0f, 1.0f, 0.0f)},  // v2
        {QVector3D( 1.0f,  1.0f,  1.0f), QVector2D(0.25f, 0.66f), QVector3D(0.0f, 1.0f, 0.0f)}, // v3

        // Vertex data for face 1
        {QVector3D( 1.0f, -1.0f,  1.0f), QVector2D(0.25f, 0.33f), QVector3D(1.0f, 0.0f, 0.0f)}, // v4 - Right side
        {QVector3D( 1.0f, -1.0f, -1.0f), QVector2D(0.5f, 0.33f), QVector3D(1.0f, 0.0f, 0.0f)}, // v5
        {QVector3D( 1.0f,  1.0f,  1.0f), QVector2D(0.25f, 0.66f), QVector3D(1.0f, 0.0f, 0.0f)},  // v6
        {QVector3D( 1.0f,  1.0f, -1.0f), QVector2D(0.5f, 0.66f), QVector3D(1.0f, 0.0f, 0.0f)}, // v7

        // Vertex data for face 2
        {QVector3D( 1.0f, -1.0f, -1.0f), QVector2D(0.5f, 0.33f), QVector3D(0.0f, -1.0f, 0.0f)}, // v8 - Bottom Side
        {QVector3D(-1.0f, -1.0f, -1.0f), QVector2D(0.75f, 0.33f), QVector3D(0.0f, -1.0f, 0.0f)},  // v9
        {QVector3D( 1.0f,  1.0f, -1.0f), QVector2D(0.5f, 0.66f), QVector3D(0.0f, -1.0f, 0.0f)}, // v10
        {QVector3D(-1.0f,  1.0f, -1.0f), QVector2D(0.75f, 0.66f), QVector3D(0.0f, -1.0f, 0.0f)},  // v11

        // Vertex data for face 3
        {QVector3D(-1.0f, -1.0f, -1.0f), QVector2D(0.75f, 0.33f), QVector3D(-1.0f, 0.0f, 0.0f)}, // v12 - Left Side
        {QVector3D(-1.0f, -1.0f,  1.0f), QVector2D(1.0f, 0.33f), QVector3D(-1.0f, 0.0f, 0.0f)},  // v13
        {QVector3D(-1.0f,  1.0f, -1.0f), QVector2D(0.75f, 0.66f), QVector3D(-1.0f, 0.0f, 0.0f)}, // v14
        {QVector3D(-1.0f,  1.0f,  1.0f), QVector2D(1.0f, 0.66f), QVector3D(-1.0f, 0.0f, 0.0f)},  // v15

        // Vertex data for face 4
        {QVector3D(-1.0f, -1.0f, -1.0f), QVector2D(0.25f, 0.0f), QVector3D(0.0f, 0.0f, -1.0f)}, // v16 - Back Side
        {QVector3D( 1.0f, -1.0f, -1.0f), QVector2D(0.5f, 0.0f), QVector3D(0.0f, 0.0f, -1.0f)}, // v17
        {QVector3D(-1.0f, -1.0f,  1.0f), QVector2D(0.25f, 0.33f), QVector3D(0.0f, 0.0f, -1.0f)}, // v18
        {QVector3D( 1.0f, -1.0f,  1.0f), QVector2D(0.5f, 0.33f), QVector3D(0.0f, 0.0f, -1.0f)}, // v19

        // Vertex data for face 5
        {QVector3D(-1.0f,  1.0f,  1.0f), QVector2D(0.25f, 0.66f), QVector3D(0.0f, 0.0f, 1.0f)}, // v20 - Front Side
        {QVector3D( 1.0f,  1.0f,  1.0f), QVector2D(0.5f, 0.66f), QVector3D(0.0f, 0.0f, 1.0f)}, // v21
        {QVector3D(-1.0f,  1.0f, -1.0f), QVector2D(0.25f, 1.0f), QVector3D(0.0f, 0.0f, 1.0f)}, // v22
        {QVector3D( 1.0f,  1.0f, -1.0f), QVector2D(0.5f, 1.0f), QVector3D(0.0f, 0.0f, 1.0f)}  // v23
    };



    // Indices for drawing cube faces using triangle strips.
    // Triangle strips can be connected by duplicating indices
    // between the strips. If connecting strips have opposite
    // vertex order then last index of the first strip and first
    // index of the second strip needs to be duplicated. If
    // connecting strips have same vertex order then only last
    // index of the first strip needs to be duplicated.
    unsigned int indexCount = 34; //Careful update indicesNumber when creating new geometry
    GLushort indices[] = {
         0,  1,  2,  3,  3,     // Face 0 - triangle strip ( v0,  v1,  v2,  v3)
         4,  4,  5,  6,  7,  7, // Face 1 - triangle strip ( v4,  v5,  v6,  v7)
         8,  8,  9, 10, 11, 11, // Face 2 - triangle strip ( v8,  v9, v10, v11)
        12, 12, 13, 14, 15, 15, // Face 3 - triangle strip (v12, v13, v14, v15)
        16, 16, 17, 18, 19, 19, // Face 4 - triangle strip (v16, v17, v18, v19)
        20, 20, 21, 22, 23      // Face 5 - triangle strip (v20, v21, v22, v23)
    };

//! [1]
    // Transfer vertex data to VBO 0
    arrayBuf.bind();
    arrayBuf.allocate(vertices, vertexNumber * sizeof(VertexData));

    // Transfer index data to VBO 1
    indexBuf.bind();
    indexBuf.allocate(indices,  indexCount * sizeof(GLushort));

    this->setOpenGLPrimitive(GL_TRIANGLE_STRIP);

    this->setBoundingBoxCorners(QVector3D(-1.0,-1.0,-1.0),QVector3D(1.0,1.0,1.0));
//! [1]
}

void Mesh::setBoundingBoxCorners(QVector3D firstCorner, QVector3D secondCorner){
    this->boundingBox[0] = firstCorner;
    this->boundingBox[1] = secondCorner;
}

void Mesh::initBoundingBox()
{
    // For cube we would need only 8 vertices but we have to
    // duplicate vertex for each face because texture coordinate
    // is different.
    // Hence 4 vertices per face and 6 faces vertices = 24 vertices
    unsigned int vertexNumber = 24 ;
    VertexData vertices[] = {
        // Vertex data for face 0
        {QVector3D(this->boundingBox[0][0], this->boundingBox[0][1],  this->boundingBox[1][2]), QVector2D(0.0f, 0.0f), QVector3D(0.0f, 1.0f, 0.0f)}, // v0 - Top side
        {QVector3D(this->boundingBox[1][0], this->boundingBox[0][1],  this->boundingBox[1][2]), QVector2D(0.33f, 0.0f), QVector3D(0.0f, 1.0f, 0.0f)}, // v1
        {QVector3D(this->boundingBox[0][0], this->boundingBox[1][1],  this->boundingBox[1][2]), QVector2D(0.0f, 0.5f), QVector3D(0.0f, 1.0f, 0.0f)},  // v2
        {QVector3D(this->boundingBox[1][0], this->boundingBox[1][1],  this->boundingBox[1][2]), QVector2D(0.33f, 0.5f), QVector3D(0.0f, 1.0f, 0.0f)}, // v3

        // Vertex data for face 1
        {QVector3D(this->boundingBox[1][0], this->boundingBox[0][1],  this->boundingBox[1][2]), QVector2D( 0.0f, 0.5f), QVector3D(1.0f, 0.0f, 0.0f)}, // v4 - Right side
        {QVector3D(this->boundingBox[1][0], this->boundingBox[0][1],  this->boundingBox[0][2]), QVector2D(0.33f, 0.5f), QVector3D(1.0f, 0.0f, 0.0f)}, // v5
        {QVector3D(this->boundingBox[1][0], this->boundingBox[1][1],  this->boundingBox[1][2]), QVector2D(0.0f, 1.0f), QVector3D(1.0f, 0.0f, 0.0f)},  // v6
        {QVector3D(this->boundingBox[1][0], this->boundingBox[1][1],  this->boundingBox[0][2]), QVector2D(0.33f, 1.0f), QVector3D(1.0f, 0.0f, 0.0f)}, // v7

        // Vertex data for face 2
        {QVector3D(this->boundingBox[1][0], this->boundingBox[0][1],  this->boundingBox[0][2]), QVector2D(0.66f, 0.5f), QVector3D(0.0f, -1.0f, 0.0f)}, // v8 - Bottom Side
        {QVector3D(this->boundingBox[0][0], this->boundingBox[0][1],  this->boundingBox[0][2]), QVector2D(1.0f, 0.5f), QVector3D(0.0f, -1.0f, 0.0f)},  // v9
        {QVector3D(this->boundingBox[1][0], this->boundingBox[1][1],  this->boundingBox[0][2]), QVector2D(0.66f, 1.0f), QVector3D(0.0f, -1.0f, 0.0f)}, // v10
        {QVector3D(this->boundingBox[0][0], this->boundingBox[1][1],  this->boundingBox[0][2]), QVector2D(1.0f, 1.0f), QVector3D(0.0f, -1.0f, 0.0f)},  // v11

        // Vertex data for face 3
        {QVector3D(this->boundingBox[0][0], this->boundingBox[0][1],  this->boundingBox[0][2]), QVector2D(0.66f, 0.0f), QVector3D(-1.0f, 0.0f, 0.0f)}, // v12 - Left Side
        {QVector3D(this->boundingBox[0][0], this->boundingBox[0][1],  this->boundingBox[1][2]), QVector2D(1.0f, 0.0f), QVector3D(-1.0f, 0.0f, 0.0f)},  // v13
        {QVector3D(this->boundingBox[0][0], this->boundingBox[1][1],  this->boundingBox[0][2]), QVector2D(0.66f, 0.5f), QVector3D(-1.0f, 0.0f, 0.0f)}, // v14
        {QVector3D(this->boundingBox[0][0], this->boundingBox[1][1],  this->boundingBox[1][2]), QVector2D(1.0f, 0.5f), QVector3D(-1.0f, 0.0f, 0.0f)},  // v15

        // Vertex data for face 4
        {QVector3D(this->boundingBox[0][0], this->boundingBox[0][1],  this->boundingBox[0][2]), QVector2D(0.33f, 0.0f), QVector3D(0.0f, 0.0f, -1.0f)}, // v16 - Back Side
        {QVector3D(this->boundingBox[1][0], this->boundingBox[0][1],  this->boundingBox[0][2]), QVector2D(0.66f, 0.0f), QVector3D(0.0f, 0.0f, -1.0f)}, // v17
        {QVector3D(this->boundingBox[0][0], this->boundingBox[0][1],  this->boundingBox[1][2]), QVector2D(0.33f, 0.5f), QVector3D(0.0f, 0.0f, -1.0f)}, // v18
        {QVector3D(this->boundingBox[1][0], this->boundingBox[0][1],  this->boundingBox[1][2]), QVector2D(0.66f, 0.5f), QVector3D(0.0f, 0.0f, -1.0f)}, // v19

        // Vertex data for face 5
        {QVector3D(this->boundingBox[0][0], this->boundingBox[1][1],  this->boundingBox[1][2]), QVector2D(0.33f, 0.5f), QVector3D(0.0f, 0.0f, 1.0f)}, // v20 - Front Side
        {QVector3D(this->boundingBox[1][0], this->boundingBox[1][1],  this->boundingBox[1][2]), QVector2D(0.66f, 0.5f), QVector3D(0.0f, 0.0f, 1.0f)}, // v21
        {QVector3D(this->boundingBox[0][0], this->boundingBox[1][1],  this->boundingBox[0][2]), QVector2D(0.33f, 1.0f), QVector3D(0.0f, 0.0f, 1.0f)}, // v22
        {QVector3D(this->boundingBox[1][0], this->boundingBox[1][1],  this->boundingBox[0][2]), QVector2D(0.66f, 1.0f), QVector3D(0.0f, 0.0f, 1.0f)}  // v23
    };



    // Indices for drawing cube faces using triangle strips.
    // Triangle strips can be connected by duplicating indices
    // between the strips. If connecting strips have opposite
    // vertex order then last index of the first strip and first
    // index of the second strip needs to be duplicated. If
    // connecting strips have same vertex order then only last
    // index of the first strip needs to be duplicated.
    unsigned int indexCount = 34; //Careful update indicesNumber when creating new geometry
    GLushort indices[] = {
         0,  1,  2,  3,  3,     // Face 0 - triangle strip ( v0,  v1,  v2,  v3)
         4,  4,  5,  6,  7,  7, // Face 1 - triangle strip ( v4,  v5,  v6,  v7)
         8,  8,  9, 10, 11, 11, // Face 2 - triangle strip ( v8,  v9, v10, v11)
        12, 12, 13, 14, 15, 15, // Face 3 - triangle strip (v12, v13, v14, v15)
        16, 16, 17, 18, 19, 19, // Face 4 - triangle strip (v16, v17, v18, v19)
        20, 20, 21, 22, 23      // Face 5 - triangle strip (v20, v21, v22, v23)
    };

//! [1]
    // Transfer vertex data to VBO 0
    arrayBuf.bind();
    arrayBuf.allocate(vertices, vertexNumber * sizeof(VertexData));

    // Transfer index data to VBO 1
    indexBuf.bind();
    indexBuf.allocate(indices,  indexCount * sizeof(GLushort));

    //This isnt perfect but enough to debug, it was meant for a triangle strip
    this->setOpenGLPrimitive(GL_LINES);
//! [1]
}

void Mesh::initPlaneGeometry(long lNbW, long lNbH, QImage* heightmap)
{
    // For this plane we have to draw nxm vertices

    unsigned int vertexNumber = lNbW * lNbH;
    VertexData vertices[vertexNumber] = {};


    // Pour la heightmap -> Lire l'image :
    /*
     * - Pour récupérer la texture
     *
     * - On récupère les tailles de la texture
     * width = img.width();
     * height = img.height();
     *
     * - On récupère le tableau de pixels pour pouvoir le lire et appliquer cela aux points du plan
     * QImage img = QImage(QString(":/heightmap.png"));
     * QRgb color = img.pixel(x, y);
    */

    float fHeight = 0;
    float fMaxHeight = 1;

    //The plane goes from -1 to 1 on both axis by default. That gives a distance between both sides of 2
    float fHStep = 2.0 / (lNbH-1);
    float fWStep = 2.0 / (lNbW-1);

    //Positions used for the bounding box
    float minX = std::numeric_limits<float>::max();
    float minY = std::numeric_limits<float>::max();
    float minZ = std::numeric_limits<float>::max();
    float maxX = std::numeric_limits<float>::min();
    float maxY = std::numeric_limits<float>::min();
    float maxZ = std::numeric_limits<float>::min();

    long lWIndex = 0, lHIndex = 0;
    for(; lHIndex< lNbH; lHIndex++){
        lWIndex = 0;
        for(; lWIndex < lNbW; lWIndex++){
            //Implement the heightmap here
            //Since the picture is in grayscale, we can use any channels to get the gray value
            if(heightmap != nullptr){
                int width, height, heightmapWidthStep, heightmapHeightStep;

                width = heightmap->width();
                height = heightmap->height();
                heightmapWidthStep = width / (lNbW-1);
                heightmapHeightStep = height / (lNbH-1);

                QRgb pixelColor = heightmap->pixel((lHIndex) * heightmapHeightStep, (lWIndex) * heightmapWidthStep);
                fHeight = 1-((float)qGray(pixelColor)/255.0) * fMaxHeight;
            } else {
                fHeight = 0.1;
            }

            float posX, posY, posZ;
            posX = -1.0f + lHIndex * fHStep;
            posY = fHeight;
            posZ = -1.0f + lWIndex * fWStep;

            minX = std::min(minX,posX);
            minY = std::min(minY,posY);
            minZ = std::min(minZ,posZ);

            maxX = std::max(maxX,posX);
            maxY = std::max(maxY,posY);
            maxZ = std::max(maxZ,posZ);

            vertices[lHIndex*lNbW + lWIndex] = {QVector3D(posX, posY, posZ), QVector2D((lHIndex * fHStep)/2.0f, (lWIndex * fWStep)/2.0f), QVector3D(0.0f, 1.0f, 0.0f)};
        }
    }

    //Set bounding box to correct values
    this->setBoundingBoxCorners(QVector3D(minX,minY,minZ), QVector3D(maxX,maxY,maxZ));


    //Number of index of the plane -> Given by doing [(Width * 2 + 2) * (Height - 1)]
    unsigned int indexCount = (lNbW * 2 + 2) * (lNbH - 1);
    GLushort indices[indexCount] = {};

    lWIndex = 0, lHIndex = 0;
    long lIndicesIndex = 0;
    for(; lHIndex< lNbH-1; lHIndex++){
        lWIndex = 0;
        for(; lWIndex< lNbW; lWIndex++){
            //1st point
            indices[lIndicesIndex] = lHIndex*lNbW + lWIndex;
            lIndicesIndex++;
            //If we're on the first point of this line add one more point to generate a degenerate triangle (and prevent artifacts)
            if (lWIndex == 0){
                indices[lIndicesIndex] = lHIndex*lNbW + lWIndex;
                lIndicesIndex++;
            }
            //2nd point -> We go down one point
            lHIndex++;
            indices[lIndicesIndex] = lHIndex*lNbW + lWIndex;
            lIndicesIndex++;
            //If we're on the last point of this line add one more point to generate a degenerate triangle (and prevent artifacts)
            if(lWIndex == lNbW-1){
                indices[lIndicesIndex] = lHIndex*lNbW + lWIndex;
                lIndicesIndex++;
            }
            //We return one point above before going to the right
            lHIndex--;
        }
    }

//! [1]
    // Transfer vertex data to VBO 0
    arrayBuf.bind();
    arrayBuf.allocate(vertices, vertexNumber * sizeof(VertexData));

    // Transfer index data to VBO 1
    indexBuf.bind();
    indexBuf.allocate(indices,  indexCount* sizeof(GLushort));

    this->setOpenGLPrimitive(GL_TRIANGLE_STRIP);
//! [1]
}


//! [2]
void Mesh::draw(QOpenGLShaderProgram *program)
{
    if(this->meshTexture != nullptr){
        this->meshTexture->bind();
    }

    // Tell OpenGL which VBOs to use
    arrayBuf.bind();
    indexBuf.bind();

    // Offset for position
    quintptr offset = 0;

    // Tell OpenGL programmable pipeline how to locate vertex position data
    int vertexLocation = program->attributeLocation("a_position");
    program->enableAttributeArray(vertexLocation);
    program->setAttributeBuffer(vertexLocation, GL_FLOAT, offset, 3, sizeof(VertexData));

    // Offset for texture coordinate
    offset += sizeof(QVector3D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int texcoordLocation = program->attributeLocation("a_texcoord");
    program->enableAttributeArray(texcoordLocation);
    program->setAttributeBuffer(texcoordLocation, GL_FLOAT, offset, 2, sizeof(VertexData));

    // Offset for normals
    offset += sizeof(QVector2D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int normalLocation = program->attributeLocation("a_normal");
    program->enableAttributeArray(normalLocation);
    program->setAttributeBuffer(normalLocation, GL_FLOAT, offset, 3, sizeof(VertexData));

    if(this->getMaterial()!= nullptr){
        program->setUniformValue("materialAmbient", this->getMaterial()->ambient);
        program->setUniformValue("materialDiffuse", this->getMaterial()->diffuse);
        program->setUniformValue("materialSpecular", this->getMaterial()->specular);
        program->setUniformValue("materialShininess", this->getMaterial()->shininess);
    }
    program->setUniformValue("mixCoeff", this->materialCoeff);


    // Draw cube geometry using indices from VBO 1
    glDrawElements(this->openGLPrimitive, indexBuf.size(), GL_UNSIGNED_SHORT, 0); //Careful update indicesNumber when creating new geometry

    if(this->meshTexture != nullptr){
        this->meshTexture->release();
    }
}
//! [2]

//Loads a mesh from OBJ or OFF file
void Mesh::loadMesh(const std::string & filename){
    //Tokenize the filename with the dot as a separator
    std::stringstream sStream(filename);
    std::string token;
    while(getline(sStream, token, '.')){}

    //If the file extension isn't obj then send an error
    if(token != "obj"){
        std::cerr<<"Erreur : Mesh :: type de fichier invalide (doit etre en .obj)"<<std::endl;
        return;
    }


    //Positions used for the bounding box
    float minX = std::numeric_limits<float>::max();
    float minY = std::numeric_limits<float>::max();
    float minZ = std::numeric_limits<float>::max();
    float maxX = std::numeric_limits<float>::min();
    float maxY = std::numeric_limits<float>::min();
    float maxZ = std::numeric_limits<float>::min();

    //TinyObjLoader -------------------------------------------------------

    tinyobj::ObjReaderConfig reader_config;
    reader_config.mtl_search_path = "./"; // Path to material files

    tinyobj::ObjReader reader;

    if (!reader.ParseFromFile(filename, reader_config)) {
      if (!reader.Error().empty()) {
          std::cerr << "TinyObjReader: " << reader.Error();
      }
      exit(1);
    }

    if (!reader.Warning().empty()) {
      std::cout << "TinyObjReader: " << reader.Warning();
    }

    auto& attrib = reader.GetAttrib();
    auto& shapes = reader.GetShapes();

    unsigned int indexCount = 0;

    std::vector<VertexData> tempVertices; //Since we don't know the global number of indices beforehand, create a dynamic array

    for (const tinyobj::shape_t& shape : shapes)
    {
        size_t triangleCount = shape.mesh.num_face_vertices.size();

        indexCount += (triangleCount * 3);

        size_t index = 0;
        for (size_t f = 0; f < triangleCount; ++f)
        {
            assert(shape.mesh.num_face_vertices[f] == 3);
            for (size_t j = 0; j < 3; ++j)
            {
                VertexData vData;
                const tinyobj::index_t& i = shape.mesh.indices[index];
                vData.position = QVector3D(attrib.vertices[3 * i.vertex_index + 0],
                                      attrib.vertices[3 * i.vertex_index + 1],
                                      attrib.vertices[3 * i.vertex_index + 2]);

                vData.normal = QVector3D(attrib.normals[3 * i.normal_index + 0],
                                      attrib.normals[3 * i.normal_index + 1],
                                      attrib.normals[3 * i.normal_index + 2]);

                vData.texCoord = QVector2D(attrib.texcoords[2 * i.texcoord_index + 0],
                                           attrib.texcoords[2 * i.texcoord_index + 1]);

                tempVertices.push_back(vData);
                ++index;
            }
        }
    }

    unsigned int vertexNumber = tempVertices.size();

    VertexData finalVertices[vertexNumber] = {};
    GLushort finalIndices[indexCount] = {};

    for(unsigned int vIndex = 0; vIndex < tempVertices.size(); vIndex++){
        finalVertices[vIndex] = tempVertices[vIndex];

        //Get bounding box corners
        minX = std::min(minX,finalVertices[vIndex].position[0]);
        minY = std::min(minY,finalVertices[vIndex].position[1]);
        minZ = std::min(minZ,finalVertices[vIndex].position[2]);

        maxX = std::max(maxX,finalVertices[vIndex].position[0]);
        maxY = std::max(maxY,finalVertices[vIndex].position[1]);
        maxZ = std::max(maxZ,finalVertices[vIndex].position[2]);
    }

    //Set bounding box to correct values
    this->setBoundingBoxCorners(QVector3D(minX,minY,minZ), QVector3D(maxX,maxY,maxZ));

    for (GLushort i = 0; (unsigned int) i < indexCount; ++i){
        finalIndices[i] = i;
    }

    // Transfer vertex data to VBO 0
    arrayBuf.bind();
    arrayBuf.allocate(finalVertices, vertexNumber * sizeof(VertexData));

    // Transfer index data to VBO 1
    indexBuf.bind();
    indexBuf.allocate(finalIndices,  indexCount* sizeof(GLushort));

    this->setOpenGLPrimitive(GL_TRIANGLES);
}

void Mesh::setMaterial(Material* in_material){
    this->meshMaterial = in_material;
}

void Mesh::setMaterialCoeff(float in_coeff){
    this->materialCoeff = in_coeff;
}

void Mesh::setOpenGLPrimitive(int in_openGLPrimitive){
    this->openGLPrimitive = in_openGLPrimitive;
}

void Mesh::setTexture(QOpenGLTexture* in_texture){
    this->meshTexture = in_texture;
}

QVector3D Mesh::getBoundingBoxPos(int index){
    return this->boundingBox[index];
}

Material* Mesh::getMaterial(){
    return this->meshMaterial;
}
