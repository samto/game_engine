#ifndef COMPONENT_H
#define COMPONENT_H

#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QRandomGenerator>
#include <QOpenGLTexture>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <QVector2D>
#include <QVector3D>
#include <QString>
#include <QImage>
#include <QFile>
#include <QRegularExpression>
#include <vector>
#include <QMatrix4x4>
#include <QGenericMatrix>
#include <QQuaternion>
#include <limits>

class Component
{
private:
    static int counterID;
    const int componentID;
    QString name = "UNDEFINED";

protected:
    ~Component();

public:
    Component();
    Component(QString name);

    //Getters
    QString getName();
    int getID();

    //Accessors
    virtual void setName(QString name);
};

#endif // COMPONENT_H
