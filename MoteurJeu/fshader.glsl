#version 140
//La version 330 ne marche pas avec X2Go
//#version 330 core

uniform float mixCoeff; //0 to show the texture, 1 to show the color, in between to mix both
uniform vec3 materialAmbient;
uniform vec3 materialDiffuse;
uniform vec3 materialSpecular;
uniform float materialShininess;
uniform vec4 lightColor;
uniform sampler2D texture;

in vec2 v_texcoord;
in vec3 v_position;
in vec3 v_normal;
in vec3 light_pos;
in vec3 camera_pos;

out vec4 FragColor;

//! [0]
void main()
{
    //Ambient
    vec3 ambient = lightColor.xyz * vec3(mix(texture2D(texture, v_texcoord),vec4(materialAmbient,1.0f),mixCoeff));

    //Diffuse
    vec3 normal = normalize(v_normal);
    vec3 lightDir = normalize(light_pos - v_position);
    float diff = max(dot(normal, lightDir), 0.0f);
    vec3 diffuse = lightColor.xyz * (diff * materialDiffuse);

    //Specular
    vec3 viewDir = normalize(camera_pos - v_position);
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), materialShininess);
    vec3 specular = lightColor.xyz * (spec * materialSpecular);

    vec3 resultColor = ambient + diffuse + specular;
    FragColor = vec4(resultColor, 1.0);

}
//! [0]

