#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <QString>
#include <vector>
#include <set>
#include "component.h"
#include "Transform.h"
#include "camera.h"
#include "mesh.h"
#include "lighting.h"
#include "boundingbox.h"


class GameObject{
private:
    static int counterID;
    const int objectID;
    QString name = "UNDEFINED";

    //Boolean to define whether the object is a scene or not
    bool isRoot = false;
    GameObject* parent = nullptr;
    std::vector<GameObject*> children = std::vector<GameObject*>();
    std::vector<Component*> components = std::vector<Component*>();

public:
    GameObject (bool in_IsRoot = false);
    GameObject (QString name, bool in_IsRoot = false);
    ~GameObject();

    //Getters
    QString getName();
    int getID();
    GameObject* getParent();
    GameObject* getChildrenByIndex(int index);
    GameObject* getChildrenByID(int ID);
    GameObject* getChildrenByName(QString name); //If two names are duplicates gets the 1st occurence
    unsigned int getChildCount();
    void update(QOpenGLShaderProgram *program, bool in_debugMode = false);
    void updateCameraWindowSize(QVector2D windowSize);
    void updatePhysics();

    void getAllCollisionsBoxes(std::vector<BoundingBox*> *out_boundingBoxList);

    template<typename ComponentType>
    ComponentType* getComponent(){
        ComponentType* cType = nullptr;

        for(auto index : components){
            if((cType = dynamic_cast<ComponentType*>(index)) != nullptr)
                break;
        }

        return cType;
    }
    unsigned int getComponentCount();

    //Accessors
    void addChild(GameObject* child);
    void removeChildByID(int ID);
    void removeChildByName(QString name);
    void setParent(GameObject* parent);
    void setName(QString name);

    template<typename ComponentType>
    void addComponent(ComponentType* component){
        //Check if the component isnt there already
        ComponentType* cType = nullptr;
        bool compExists = false;

        for(auto index : components){
            if((cType = dynamic_cast<ComponentType*>(index)) != nullptr){
                compExists = true;
                break;
            }
        }

        if(compExists){
            std::cout<<"Please remove the existing component before adding a new one"<<std::endl;
            return;
        }

        //Check if the type of the template is a transform, if it is add the parent transform if it is possible
        const std::type_info& tiTransform = typeid(Transform);
        const std::type_info& tiComponentType = typeid(ComponentType);

        if(tiTransform.hash_code() == tiComponentType.hash_code()){
            if(parent != nullptr){
                Transform* parentTransform = parent->getComponent<Transform>();
                if(parentTransform != nullptr){
                    Transform* transform = dynamic_cast<Transform*>(component);
                    transform->setParent(parentTransform);
                }
            }
        }

        this->components.push_back(component);
    }

    template<typename ComponentType>
    void removeComponent(){
        ComponentType* cType = nullptr;
        int indexComp = -1;

        for(unsigned int index = 0; index < getComponentCount(); index++){
            if((cType = dynamic_cast<ComponentType*>(components[index])) != nullptr){
                indexComp = index;
                break;
            }
        }

        if(indexComp >= 0)
            this->components.erase(this->components.begin()+indexComp);
    }
};

#endif // GAMEOBJECT_H
