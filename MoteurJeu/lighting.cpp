#include "lighting.h"

Lighting::Lighting() : Component("Lighting"){
}

Lighting::Lighting(Transform* in_linkedTransform, QVector4D in_lightColor) : Component("Lighting"){
    setLinkedTransform(in_linkedTransform);
    setLightColor(in_lightColor);
}

Lighting::~Lighting(){
}

void Lighting::setLightColor(QVector4D in_lightColor){
    this->lightColor = in_lightColor;
}

QVector4D Lighting::getLightColor(){
    return this->lightColor;
}

void Lighting::setLinkedTransform(Transform* in_linkedTransform){
    this->linkedTransform = in_linkedTransform;
}
