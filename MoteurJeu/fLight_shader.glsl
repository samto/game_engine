#version 140

uniform float mixCoeff; //0 to show the texture, 1 to show the color, in between to mix both
uniform vec4 materialColor;
uniform sampler2D texture;
//layout(binding = 1) uniform sampler2D rockTexture;
//layout(binding = 2) uniform sampler2D snowTexture;

in vec2 v_texcoord;
in vec4 v_position;
//! [0]
void main()
{
    // Set fragment color from texture
    gl_FragColor = mix(texture2D(texture, v_texcoord),materialColor,mixCoeff);
    //vec4(v_position.x, v_position.y, v_position.z, 1.0);// texture2D(texture, v_texcoord);

    /*
    if(v_position.z < 0.4){
        gl_FragColor = texture2D(grassTexture, v_texcoord);
    } else if(v_position.z < 0.75){
        gl_FragColor = texture2D(rockTexture, v_texcoord);
    } else {
        gl_FragColor = texture2D(snowTexture, v_texcoord);
    }
    */

}
//! [0]

