#include "GameObject.h"

int GameObject::counterID = 0;

GameObject::GameObject(bool in_IsRoot) : objectID(counterID++), isRoot(in_IsRoot){
}

GameObject::GameObject(QString name, bool in_IsRoot) : objectID(counterID++), isRoot(in_IsRoot){
    this->setName(name);
}

GameObject::~GameObject(){
    parent = nullptr;
}

QString GameObject::getName(){
    return this->name;
}

int GameObject::getID(){
    return this->objectID;
}

GameObject* GameObject::getParent(){
    return this->parent;
}

GameObject* GameObject::getChildrenByIndex(int index){
    return this->children[index];
}

GameObject* GameObject::getChildrenByID(int ID){
    unsigned int cptChild = 0;
    bool childFound = false;
    GameObject* child = nullptr;
    while(cptChild < this->getChildCount() && !childFound){
        if(getChildrenByIndex(cptChild)->getID() == ID){
            child = this->children[cptChild];
            childFound = true;
        }
        cptChild++;
    }

    return child;
}

GameObject* GameObject::getChildrenByName(QString name){
    unsigned int cptChild = 0;
    bool childFound = false;
    GameObject* child = nullptr;
    while(cptChild < this->getChildCount() && !childFound){
        if(getChildrenByIndex(cptChild)->getName() == name){
            child = this->children[cptChild];
            childFound = true;
        }
        cptChild++;
    }

    return child;
}

unsigned int GameObject::getChildCount(){
    return this->children.size();
}

void GameObject::update(QOpenGLShaderProgram *program, bool in_debugMode){


    //Get all required components
    Mesh* meshComponent = this->getComponent<Mesh>();
    Transform* transformComponent = this->getComponent<Transform>();
    Camera* cameraComponent = this->getComponent<Camera>();
    Lighting* lightingComponent = this->getComponent<Lighting>();
    BoundingBox* bbComponent = this->getComponent<BoundingBox>();

    //Update meshes and draw them
    if(meshComponent != nullptr && transformComponent != nullptr){
        //Send the transform matrix of this mesh to the shader
        QMatrix4x4 transformMatrix = transformComponent->getGlobalTransformation();
        program->setUniformValue("Model", transformMatrix);

        //Draw the mesh
        meshComponent->draw(program);
    }

    //Update the camera
    if(cameraComponent != nullptr && transformComponent != nullptr){
        //Send the View & Projection Matrix to the shader
        program->setUniformValue("View", cameraComponent->View());
        program->setUniformValue("Projection", cameraComponent->Projection());
        program->setUniformValue("cameraPos", transformComponent->getPosition());
    }

    //Update the lighting
    if(lightingComponent != nullptr && transformComponent != nullptr){
        //Send the View & Projection Matrix to the shader
        program->setUniformValue("lightColor", lightingComponent->getLightColor());
        program->setUniformValue("lightPos", transformComponent->getPosition());
    }

    //If the Debug mode is ON and the component has a bouding box draw it
    if(bbComponent != nullptr && in_debugMode){
        bbComponent->drawDebugBoundingBox(program);
    }

    if(this->getChildCount() > 0){
        for(unsigned int cptChild = 0; cptChild < this->getChildCount(); cptChild++){
            this->getChildrenByIndex(cptChild)->update(program, in_debugMode);
        }
    }
}

void GameObject::updateCameraWindowSize(QVector2D windowSize){
    if(this->getChildCount() > 0){
        for(unsigned int cptChild = 0; cptChild < this->getChildCount(); cptChild++){
            this->getChildrenByIndex(cptChild)->updateCameraWindowSize(windowSize);
        }
    }

    //Get all required components
    Camera* cameraComponent = this->getComponent<Camera>();

    //Update the camera
    if(cameraComponent != nullptr){
        //Update the window size
        cameraComponent->setWindowSize(windowSize);
    }
}

void GameObject::getAllCollisionsBoxes(std::vector<BoundingBox*> *out_boundingBoxList){
    if(this->getChildCount() > 0){
        for(unsigned int cptChild = 0; cptChild < this->getChildCount(); cptChild++){
            this->getChildrenByIndex(cptChild)->getAllCollisionsBoxes(out_boundingBoxList);
        }
    }

    //Get all required components
    BoundingBox* bbComponent = this->getComponent<BoundingBox>();

    if(bbComponent != nullptr){
        out_boundingBoxList->push_back(bbComponent);
    }
}

void GameObject::updatePhysics(){
    std::vector<BoundingBox*> bbList = std::vector<BoundingBox*>();

    //Retrieve all bounding boxes
    this->getAllCollisionsBoxes(&bbList);

    //Compute collisions
    //On X-axis -----------------------------------------------------------
    std::vector<bbPoint> bbPoints = std::vector<bbPoint>();
    for(unsigned int cptBB = 0; cptBB < bbList.size(); cptBB++){
        //Reset collisions (not optimized)
        bbList[cptBB]->clearCollisions();
        bbPoints.push_back(bbList[cptBB]->getStartPoint());
        bbPoints.push_back(bbList[cptBB]->getEndPoint());
    }


    //Sort values in X-axis order
    std::sort(bbPoints.begin(),bbPoints.end(),[ ]( const bbPoint& bbP1, const bbPoint& bbP2 )
    {
       return bbP1.position[0] < bbP2.position[0];
    });

    std::vector<BoundingBox*> echeancier = std::vector<BoundingBox*>();
    std::set<std::pair<BoundingBox*,BoundingBox*>> xCollision = std::set<std::pair<BoundingBox*,BoundingBox*>>();
    int indexEch;
    for(unsigned int cptPoints = 0; cptPoints < bbPoints.size(); cptPoints++){
        indexEch = -1;
        for(unsigned int cptEch = 0; cptEch < echeancier.size(); cptEch++){
            if(echeancier[cptEch] == bbPoints[cptPoints].linkedBB){
                indexEch = cptEch;
                break;
            }
        }
        //If the point hasn't been seen yet then add it to the echeancier
        if(indexEch == -1){
            if(echeancier.size()>=1){
                for(unsigned int cptEch = 0; cptEch < echeancier.size(); cptEch++){
                    //Make sure to sort by adress value to be sure pair are in the same order
                    if(bbPoints[cptPoints].linkedBB < bbPoints[cptEch].linkedBB){
                        xCollision.insert(std::make_pair(bbPoints[cptPoints].linkedBB,bbPoints[cptEch].linkedBB));
                    } else {
                        xCollision.insert(std::make_pair(bbPoints[cptEch].linkedBB,bbPoints[cptPoints].linkedBB));
                    }
                }
            }
            echeancier.push_back(bbPoints[cptPoints].linkedBB);
        }
        //Else remove it from the echeancier
        else {
            echeancier.erase(echeancier.begin()+indexEch);
        }
    }



    //On Z-axis -----------------------------------------------------------

    //Sort values in Z-axis order
    std::sort(bbPoints.begin(),bbPoints.end(),[ ]( const bbPoint& bbP1, const bbPoint& bbP2 )
    {
       return bbP1.position[2] < bbP2.position[2];
    });

    echeancier = std::vector<BoundingBox*>();

    std::set<std::pair<BoundingBox*,BoundingBox*>> zCollision = std::set<std::pair<BoundingBox*,BoundingBox*>>();

    for(unsigned int cptPoints = 0; cptPoints < bbPoints.size(); cptPoints++){
        indexEch = -1;
        for(unsigned int cptEch = 0; cptEch < echeancier.size(); cptEch++){
            if(echeancier[cptEch] == bbPoints[cptPoints].linkedBB){
                indexEch = cptEch;
                break;
            }
        }
        //If the point hasn't been seen yet then add it to the echeancier
        if(indexEch == -1){
            if(echeancier.size()>=1){
                for(unsigned int cptEch = 0; cptEch < echeancier.size(); cptEch++){
                    //Make sure to sort by adress value to be sure pair are in the same order
                    if(bbPoints[cptPoints].linkedBB < bbPoints[cptEch].linkedBB){
                        zCollision.insert(std::make_pair(bbPoints[cptPoints].linkedBB,bbPoints[cptEch].linkedBB));
                    } else {
                        zCollision.insert(std::make_pair(bbPoints[cptEch].linkedBB,bbPoints[cptPoints].linkedBB));
                    }
                }
            }
            echeancier.push_back(bbPoints[cptPoints].linkedBB);
        }
        //Else remove it from the echeancier
        else {
            echeancier.erase(echeancier.begin()+indexEch);
        }
    }

    //On Y-axis -----------------------------------------------------------

    //Sort values in Z-axis order
    std::sort(bbPoints.begin(),bbPoints.end(),[ ]( const bbPoint& bbP1, const bbPoint& bbP2 )
    {
       return bbP1.position[1] < bbP2.position[1];
    });

    echeancier = std::vector<BoundingBox*>();

    std::set<std::pair<BoundingBox*,BoundingBox*>> yCollision = std::set<std::pair<BoundingBox*,BoundingBox*>>();

    for(unsigned int cptPoints = 0; cptPoints < bbPoints.size(); cptPoints++){
        indexEch = -1;
        for(unsigned int cptEch = 0; cptEch < echeancier.size(); cptEch++){
            if(echeancier[cptEch] == bbPoints[cptPoints].linkedBB){
                indexEch = cptEch;
                break;
            }
        }
        //If the point hasn't been seen yet then add it to the echeancier
        if(indexEch == -1){
            if(echeancier.size()>=1){
                for(unsigned int cptEch = 0; cptEch < echeancier.size(); cptEch++){
                    //Make sure to sort by adress value to be sure pair are in the same order
                    if(bbPoints[cptPoints].linkedBB < bbPoints[cptEch].linkedBB){
                        yCollision.insert(std::make_pair(bbPoints[cptPoints].linkedBB,bbPoints[cptEch].linkedBB));
                    } else {
                        yCollision.insert(std::make_pair(bbPoints[cptEch].linkedBB,bbPoints[cptPoints].linkedBB));
                    }
                }
            }
            echeancier.push_back(bbPoints[cptPoints].linkedBB);
        }
        //Else remove it from the echeancier
        else {
            echeancier.erase(echeancier.begin()+indexEch);
        }
    }

    int nbColl = 0;

    //Check if all three axis have a pair of identic collision
    for(unsigned int xIndex = 0; xIndex < xCollision.size(); xIndex++){
        for(unsigned int yIndex = 0; yIndex < yCollision.size(); yIndex++){
            for(unsigned int zIndex = 0; zIndex < zCollision.size(); zIndex++){
                std::pair<BoundingBox*,BoundingBox*> xCol = (*std::next(xCollision.begin(), xIndex));
                std::pair<BoundingBox*,BoundingBox*> yCol = (*std::next(yCollision.begin(), yIndex));
                std::pair<BoundingBox*,BoundingBox*> zCol = (*std::next(zCollision.begin(), zIndex));

                //If a collision occurs on all axis then add its collisions to the bounding boxes
                if((xCol.first == yCol.first && yCol.first == zCol.first)
                && (xCol.second == yCol.second && yCol.second == zCol.second)){

                    xCol.first->addCollision(xCol.second);
                    xCol.second->addCollision(xCol.first);
                    nbColl++;
                }
            }
        }
    }
}

//GameObject::getComponent<ComponentType> defined in header (since its using a template)

unsigned int GameObject::getComponentCount(){
    return this->components.size();
}

void GameObject::addChild(GameObject* child){
    //Check if the child isnt there already
    for(unsigned int cptChild = 0; cptChild < this->getChildCount(); cptChild++){
        if(getChildrenByID(child->getID()) != nullptr){
            return;
        }
    }

    //Else
    this->children.push_back(child);
    child->setParent(this);
}

void GameObject::removeChildByID(int ID){
    unsigned int cptChild = 0;
    bool childFound = false;
    while(cptChild < this->getChildCount() && !childFound){
        if(getChildrenByIndex(cptChild)->getID() == ID){
            this->children.erase(this->children.begin()+cptChild);
            childFound = true;
        }
        cptChild++;
    }
}

void GameObject::removeChildByName(QString name){
    unsigned int cptChild = 0;
    bool childFound = false;
    while(cptChild < this->getChildCount() && !childFound){
        if(getChildrenByIndex(cptChild)->getName() == name){
            this->children.erase(this->children.begin()+cptChild);
            childFound = true;
        }
        cptChild++;
    }
}

void GameObject::setParent(GameObject* parent){
    //If the gameObject is the scene dont allow to a parent
    if(isRoot){
        return;
    }

    if(this->parent != nullptr){
        this->parent->removeChildByID(this->getID());
    }

    if(parent != nullptr){
        //Transform component
        Transform* parentTransform = parent->getComponent<Transform>();
        if(parentTransform != nullptr){
            Transform* transform = this->getComponent<Transform>();
            if(transform != nullptr){
                transform->setParent(parentTransform);
            }
            //If no transform component exists and the parent has one add one to this gameObject
            else {
                Transform* newTransform = new Transform(parentTransform);
                this->addComponent<Transform>(newTransform);
            }
        }
    }

    this->parent = parent;
    this->parent->children.push_back(this);
}

void GameObject::setName(QString name){
    this->name = name;
}

//GameObject::addComponent<ComponentType>(Component* component) defined in header (since its using a template)

//GameObject::removeComponent<ComponentType>() defined in header (since its using a template)
