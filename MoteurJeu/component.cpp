#include "component.h"

int Component::counterID = 0;

Component::Component() : componentID(counterID++){
}

Component::Component(QString name) : componentID(counterID++){
    this->setName(name);
}

Component::~Component(){
}

QString Component::getName(){
    return this->name;
}

int Component::getID(){
    return this->componentID;
}

void Component::setName(QString name){
    this->name = name;
}
