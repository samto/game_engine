#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "component.h"

class Transform : public Component{
private:
    Transform*  m_parent;
    QMatrix4x4   m_parentMatrix;

    QVector3D   m_pos;
    QQuaternion m_rot;
    QVector3D   m_scale;

    QVector3D   m_oldPos;
    QQuaternion m_oldRot;
    QVector3D   m_oldScale;

public:
    Transform();
    Transform(Transform* in_parentTransform);
    ~Transform();

    QMatrix4x4 getLocalTransformation();
    QMatrix4x4 getLocalInverseTransformation();
    QMatrix4x4 getGlobalTransformation();
    QMatrix4x4 getGlobalInverseTransformation();
    void setParent(Transform* parent);

    QVector3D applyToPoint(QVector3D in_point);
    QVector3D applyToVector(QVector3D in_vector);

    QVector3D getTransformedPos();
    QQuaternion getTransformedRot();

    //Uniform scaling
    void scale(float scale);
    void translate(QVector3D translation);
    void setPosition(QVector3D position);
    void rotate(QVector3D eulerAngles);
    void setRotation(QVector3D eulerAngles);

    QVector3D getPosition();
    QQuaternion getRotation();
    QVector3D getScale();
};

QVector3D multiplyMatrix(QMatrix4x4 in_matrix, QVector3D in_vector);

#endif // TRANSFORM_H
