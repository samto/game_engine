/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtCore module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef MESH_H
#define MESH_H

#include "component.h"
#include "tiny_obj_loader.h"

struct Material {
    QVector3D ambient;
    QVector3D diffuse;
    QVector3D specular;
    float shininess;

    Material(QVector3D in_ambient, QVector3D in_diffuse, QVector3D in_specular, float in_shininess) : ambient(in_ambient), diffuse(in_diffuse), specular(in_specular), shininess(in_shininess){}
};

struct VertexData
{
    QVector3D position;
    QVector2D texCoord;
    QVector3D normal;
};

class Mesh : public Component, protected QOpenGLFunctions
{
public:
    Mesh();
    virtual ~Mesh();

    void draw(QOpenGLShaderProgram *program);

    //Loads a mesh from OBJ or OFF file
    void loadMesh(const std::string & filename);

    void initCubeGeometry();

    void setBoundingBoxCorners(QVector3D firstCorner, QVector3D secondCorner);
    void initBoundingBox();

    //Initializes a plane with lNbW vertices from left to right and lNbH from bottom to top
    void initPlaneGeometry(long lNbW, long lNbH, QImage* heightmap = nullptr);

    void setMaterial(Material* in_material);
    void setMaterialCoeff(float in_coeff);
    void setOpenGLPrimitive(int in_openGLPrimitive);
    void setTexture(QOpenGLTexture* in_texture);

    QVector3D getBoundingBoxPos(int index);
    Material* getMaterial();

private:
    QOpenGLBuffer arrayBuf;
    QOpenGLBuffer indexBuf;

    int openGLPrimitive = GL_TRIANGLES;
    Material* meshMaterial = new Material(QVector3D(1.0f,1.0f,1.0f),QVector3D(0.1f,0.1f,0.1f),QVector3D(0.0f,0.0f,0.0f),1.0f);
    QOpenGLTexture* meshTexture = nullptr;
    float materialCoeff = 1.0; // K% of color to use, 100-K% of the texture will be mixed into the final color of the vertex

    QVector3D boundingBox[2];
};


#endif // MESH_H
