/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtCore module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "mainwidget.h"

#include <QMouseEvent>

#include <math.h>

MainWidget::MainWidget(QWidget *parent) :
    QOpenGLWidget(parent),
    angularSpeed(0)
{
    cameraPos = QVector3D(0,0,0);
    cameraUp  = QVector3D(0,1,0);
    cameraOrbit = true;
}

MainWidget::~MainWidget()
{
    // Make sure the context is current when deleting the texture
    // and the buffers.
    makeCurrent();

    doneCurrent();
}

//! [0]
void MainWidget::mousePressEvent(QMouseEvent *e)
{
    if(e->button() == Qt::RightButton){
        isRightMouseButtonPressed = true;
        this->setCursor(Qt::DragMoveCursor);
    } else if(e->button() == Qt::LeftButton){
        isLeftMouseButtonPressed = true;
        this->setCursor(Qt::DragMoveCursor);
    }
}

void MainWidget::mouseReleaseEvent(QMouseEvent *e)
{
    if(e->button() == Qt::RightButton){
        isRightMouseButtonPressed = false;
        this->unsetCursor();
    } else if(e->button() == Qt::LeftButton){
        isLeftMouseButtonPressed = false;
        this->unsetCursor();
    }
}

void MainWidget::mouseMoveEvent(QMouseEvent *e){
    QVector2D mouseOffset = QVector2D(e->localPos()) - QVector2D(windowSize[0]/2,windowSize[1]/2);

    float sensitivity = 0.1;
    mouseOffset *= sensitivity;

    mouseOffset.normalize();

    //Move the camera according to the mouse
    cameraRot[0] += mouseOffset[0];
    cameraRot[1] -= mouseOffset[1];

    //Avoid camera flipping upside down
    if(cameraRot[1] > 89.0f){
        cameraRot[1] = 89.0f;
    } else if(cameraRot[1] < -89.0f){
        cameraRot[1] = -89.0f;
    }

    cameraFront[0] = qCos(qDegreesToRadians(cameraRot[0])) * qCos(qDegreesToRadians(cameraRot[1]));
    cameraFront[1] = qSin(qDegreesToRadians(cameraRot[1]));
    cameraFront[2] = qSin(qDegreesToRadians(cameraRot[0])) * qCos(qDegreesToRadians(cameraRot[1]));

    cameraFront.normalize();

    m_bCameraChange = true;

    //Set Mouse position to center
    QCursor::setPos(QWidget::mapToGlobal(QPoint(windowSize[0]/2,windowSize[1]/2)));

    update();

    //Debug mouse controller
    /*
    if(isLeftMouseButtonPressed){
        cameraPos += mouseOffset[0] * QVector3D::crossProduct(cameraUp,cameraFront).normalized() * cameraSpeed;
        cameraPos += mouseOffset[1] * QVector3D::crossProduct(QVector3D::crossProduct(cameraUp,cameraFront).normalized(),-cameraFront).normalized() * cameraSpeed;

        //Set Mouse position to center
        QCursor::setPos(QWidget::mapToGlobal(QPoint(windowSize[0]/2,windowSize[1]/2)));

        m_bCameraChange = true;

        update();
    } else if(isRightMouseButtonPressed){
        cameraRot[0] += mouseOffset[0];
        cameraRot[1] -= mouseOffset[1];

        //Avoid camera flipping upside down
        if(cameraRot[1] > 89.0f){
            cameraRot[1] = 89.0f;
        } else if(cameraRot[1] < -89.0f){
            cameraRot[1] = -89.0f;
        }

        cameraFront[0] = qCos(qDegreesToRadians(cameraRot[0])) * qCos(qDegreesToRadians(cameraRot[1]));
        cameraFront[1] = qSin(qDegreesToRadians(cameraRot[1]));
        cameraFront[2] = qSin(qDegreesToRadians(cameraRot[0])) * qCos(qDegreesToRadians(cameraRot[1]));

        cameraFront.normalize();

        m_bCameraChange = true;

        //Set Mouse position to center
        QCursor::setPos(QWidget::mapToGlobal(QPoint(windowSize[0]/2,windowSize[1]/2)));

        update();
    }
    */
}

void MainWidget::wheelEvent(QWheelEvent *e)
{
    //Used for debugging ----
    /*
    float sensitivity = 0.1f;
    if(e->angleDelta().y() > 0.0f || e->angleDelta().y() < 0.0f){
        cameraPos += sensitivity * e->angleDelta().y()/8.0f * sceneCamera->getComponent<Camera>()->getViewDir();
    }
    */
    /* Zoom Method
    m_fCameraZoom -= (float)e->angleDelta().y()/8.0;

    if(m_fCameraZoom > 90.0f){
        m_fCameraZoom = 90.0f;
    } else if(m_fCameraZoom <= 0.0f){
        m_fCameraZoom = 1.0f;
    }
    */

    m_bCameraChange = true;
    update();
}
//! [0]

//! [1]
void MainWidget::timerEvent(QTimerEvent *)
{
    if(cameraOrbit){
        float cameraOrbitAngle = 2.0f;

        //Rotate monkey
        scene->getChildrenByName(QString("Suzanne_Object"))->getComponent<Transform>()->rotate(QVector3D(0.0,cameraOrbitAngle,0.0));


        update();
    }
}
//! [1]

void MainWidget::keyPressEvent(QKeyEvent *event) {
    float translationStep = 0.01;
    switch(event->key()){
        case(Qt::Key_Z):
        {
            scene->getChildrenByName(QString("Camera_Object"))->getComponent<Transform>()->translate(QVector3D(translationStep * cameraFront.x(),0.0,translationStep * cameraFront.z()));
            if(!cameraOrbit)
                update();
            break;
        }
        case(Qt::Key_S):
        {
            scene->getChildrenByName(QString("Camera_Object"))->getComponent<Transform>()->translate(QVector3D(translationStep * -cameraFront.x(),0.0,translationStep * -cameraFront.z()));
            if(!cameraOrbit)
                update();
            break;
        }
        case(Qt::Key_Q):
        {
            scene->getChildrenByName(QString("Camera_Object"))->getComponent<Transform>()->translate(QVector3D::crossProduct(cameraUp,QVector3D(cameraFront.x(),0.0f,cameraFront.z())).normalized() * translationStep);
            if(!cameraOrbit)
                update();
            break;
        }
        case(Qt::Key_D):
        {
            scene->getChildrenByName(QString("Camera_Object"))->getComponent<Transform>()->translate(QVector3D::crossProduct(cameraUp,QVector3D(cameraFront.x(),0.0f,cameraFront.z())).normalized() * -translationStep);
            if(!cameraOrbit)
                update();
            break;
        }
        case(Qt::Key_C):
        {
            cameraOrbit = !cameraOrbit;
            break;
        }
        case(Qt::Key_A):
        {
            std::cout<<"Event :: A pressed -> Debug Mode "<<!m_bDebugMode<<std::endl;
            m_bDebugMode = !m_bDebugMode;
            break;
        }
    }
}

void MainWidget::keyReleaseEvent(QKeyEvent *event){
    switch(event->key()){
        case(Qt::Key_Q):
            break;
        case(Qt::Key_D):
            break;
    }
}

void MainWidget::initializeGL()
{
    this->setMouseTracking(true);

    initializeOpenGLFunctions();

    glClearColor(0.1, 0.1, 0.3, 1);

    initShaders();
    initTextures();

//! [2]
    // Enable depth buffer
    glEnable(GL_DEPTH_TEST);

    // Enable back face culling
    glEnable(GL_CULL_FACE);

    //glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
//! [2]

    QString sceneName = QString("SCENE_Object");
    QString suzanneName = QString("Suzanne_Object");
    QString groundName = QString("Ground_Object");
    QString cameraName = QString("Camera_Object");
    QString lightName = QString("Light_Object");
    QString skyboxName = QString("Skybox_Object");

    scene = new GameObject(sceneName,true);
    GameObject* monkey = new GameObject(suzanneName);
    GameObject* ground = new GameObject(groundName);
    GameObject* MainCamera = new GameObject(cameraName);
    GameObject* light = new GameObject(lightName);

    GameObject* skyboxFront = new GameObject(QString(skyboxName+"FRONT"));
    GameObject* skyboxLeft = new GameObject(QString(skyboxName+"LEFT"));
    GameObject* skyboxBack = new GameObject(QString(skyboxName+"BACK"));
    GameObject* skyboxRight = new GameObject(QString(skyboxName+"RIGHT"));
    GameObject* skyboxTop = new GameObject(QString(skyboxName+"TOP"));
    GameObject* skyboxBottom = new GameObject(QString(skyboxName+"BOTTOM"));

    monkey->setParent(scene);
    MainCamera->setParent(scene);
    ground->setParent(scene);
    light->setParent(scene);

    skyboxFront->setParent(scene);
    skyboxLeft->setParent(scene);
    skyboxBack->setParent(scene);
    skyboxRight->setParent(scene);
    skyboxTop->setParent(scene);
    skyboxBottom->setParent(scene);

    //----------------| Materials |---------------------

    QVector3D ambient = QVector3D(0.24725f,0.1995f,0.0745f);
    QVector3D diffuse = QVector3D(0.75164f,0.6648f,0.22648f);
    QVector3D specular = QVector3D(0.628281f,0.555802f,0.366065f);
    float shininess = 0.4f * 128;
    Material* gold = new Material(ambient,diffuse,specular,shininess);

    ambient = QVector3D(0.19225f,0.19225f,0.19225f);
    diffuse = QVector3D(0.50754f,0.50754f,0.50754f);
    specular = QVector3D(0.508273f,0.508273f,0.508273f);
    shininess = 0.4f * 128;
    Material* silver = new Material(ambient,diffuse,specular,shininess);

    ambient = QVector3D(0.85f,0.8f,0.1f);
    diffuse = QVector3D(1.0f,1.0f,0.2f);
    specular = QVector3D(1.0f,1.0f,1.0f);
    shininess = 0.4f * 128;
    Material* sunMat = new Material(ambient,diffuse,specular,shininess);

    //--------------| Monkey object |-------------------

    Transform* transform = new Transform();

    transform->scale(0.1);
    transform->setRotation(QVector3D(0.0,0.0,0.0));
    transform->translate(QVector3D(0.0,0.5,0.0));

    monkey->addComponent<Transform>(transform);

    //-----| Define a texture for the mesh |-----

    QOpenGLTexture* meshTexture = new QOpenGLTexture(QImage(":/suzanne_texture.png").mirrored());
    // Set nearest filtering mode for texture minification
    meshTexture->setMinificationFilter(QOpenGLTexture::Nearest);

    // Set bilinear filtering mode for texture magnification
    meshTexture->setMagnificationFilter(QOpenGLTexture::Linear);

    // Wrap texture coordinates by repeating
    meshTexture->setWrapMode(QOpenGLTexture::Repeat);

    //-------------------------------------------

    Mesh* mesh = new Mesh();

    mesh->setMaterialCoeff(0.0);
    mesh->setTexture(meshTexture);

    mesh->loadMesh("/home/e20170000215/Documents/M2/Appli_Interactive/Projet_MoteurJeu/game_engine/MoteurJeu/suzanne.obj");

    monkey->addComponent<Mesh>(mesh);

    BoundingBox* boundingBox = new BoundingBox(mesh,transform);

    monkey->addComponent<BoundingBox>(boundingBox);

    //--------------| Ground object |-------------------

    transform = new Transform();

    transform->scale(1.0);
    transform->setRotation(QVector3D(0.0,0.0,0.0));
    transform->translate(QVector3D(0.0,0.0,0.0));

    ground->addComponent<Transform>(transform);

    mesh = new Mesh();

    mesh->setTexture(grassTexture);
    mesh->setMaterialCoeff(0.0);
   // mesh->setMaterial(gold);

    mesh->initPlaneGeometry(2,2);
    ground->addComponent<Mesh>(mesh);

    boundingBox = new BoundingBox(mesh,transform);

    ground->addComponent<BoundingBox>(boundingBox);

    //--------------| Skybox objects |-------------------

    //Front ---
    transform = new Transform();

    transform->scale(5.0);
    transform->setRotation(QVector3D(90.0,0.0,0.0));
    transform->translate(QVector3D(0.0,0.0,-5.5));

    skyboxFront->addComponent<Transform>(transform);

    mesh = new Mesh();

    mesh->setTexture(skyboxTexture[0]);
    mesh->setMaterialCoeff(0.0);

    mesh->initPlaneGeometry(2,2);
    skyboxFront->addComponent<Mesh>(mesh);

    //Right ---
    transform = new Transform();

    transform->scale(5.0);
    transform->setRotation(QVector3D(0.0,0.0,90.0));
    transform->translate(QVector3D(5.5,0.0,0.0));

    skyboxRight->addComponent<Transform>(transform);

    mesh = new Mesh();

    mesh->setTexture(skyboxTexture[3]);
    mesh->setMaterialCoeff(0.0);

    mesh->initPlaneGeometry(2,2);
    skyboxRight->addComponent<Mesh>(mesh);

    //Back ---
    transform = new Transform();

    transform->scale(5.0);
    transform->setRotation(QVector3D(-90.0,0.0,.0));
    transform->translate(QVector3D(0.0,0.0,5.5));

    skyboxBack->addComponent<Transform>(transform);

    mesh = new Mesh();

    mesh->setTexture(skyboxTexture[2]);
    mesh->setMaterialCoeff(0.0);

    mesh->initPlaneGeometry(2,2);
    skyboxBack->addComponent<Mesh>(mesh);

    //Left ---
    transform = new Transform();

    transform->scale(5.0);
    transform->setRotation(QVector3D(0.0,0.0,-90.0));
    transform->translate(QVector3D(-5.5,0.0,0.0));

    skyboxLeft->addComponent<Transform>(transform);

    mesh = new Mesh();

    mesh->setTexture(skyboxTexture[1]);
    mesh->setMaterialCoeff(0.0);

    mesh->initPlaneGeometry(2,2);
    skyboxLeft->addComponent<Mesh>(mesh);

    //Bottom ---
    transform = new Transform();

    transform->scale(5.0);
    transform->setRotation(QVector3D(0.0,0.0,0.0));
    transform->translate(QVector3D(0.0,-5.5,0.0));

    skyboxBottom->addComponent<Transform>(transform);

    mesh = new Mesh();

    mesh->setTexture(skyboxTexture[4]);
    mesh->setMaterialCoeff(0.0);

    mesh->initPlaneGeometry(2,2);
    skyboxBottom->addComponent<Mesh>(mesh);

    //Top ---
    transform = new Transform();

    transform->scale(5.0);
    transform->setRotation(QVector3D(180.0,0.0,0.0));
    transform->translate(QVector3D(0.0,5.5,0.0));

    skyboxTop->addComponent<Transform>(transform);

    mesh = new Mesh();

    mesh->setTexture(skyboxTexture[5]);
    mesh->setMaterialCoeff(0.0);

    mesh->initPlaneGeometry(2,2);
    skyboxTop->addComponent<Mesh>(mesh);

    //--------------| MainCamera object |-------------------

    transform = new Transform();

    transform->translate(QVector3D(0.0,0.5,0.5));
    transform->setRotation(QVector3D(0.0,0.0,0.0));

    MainCamera->addComponent<Transform>(transform);

    Camera* camera = new Camera(transform,windowSize);

    MainCamera->addComponent<Camera>(camera);
    sceneCamera = scene->getChildrenByName(QString("Camera_Object"));

    //--------------| Light object |-------------------

    transform = new Transform();

    transform->scale(1.0);
    transform->setRotation(QVector3D(0.0,0.0,0.0));
    transform->translate(QVector3D(1.6f,5.5f,1.6f));

    light->addComponent<Transform>(transform);

    mesh = new Mesh();

    mesh->setMaterialCoeff(1.0);
    mesh->setMaterial(sunMat);

    mesh->loadMesh("/home/e20170000215/Documents/M2/Appli_Interactive/Projet_MoteurJeu/game_engine/MoteurJeu/sphere.obj");
    light->addComponent<Mesh>(mesh);

    Lighting* lighting = new Lighting(transform,QVector4D(0.8f,0.8f,0.6f,1.0f));

    light->addComponent<Lighting>(lighting);

    //-------------------------------------------------

    // Use QBasicTimer because its faster than QTimer
    if(framerate == -1){
        timer.start(1, this);
    } else {
        timer.start(1000/framerate, this);
    }
}

//! [3]
void MainWidget::initShaders()
{
    //-------------------------------------------------------------

    // Compile vertex shader
    if (!program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/vshader.glsl"))
        close();

    // Compile fragment shader
    if (!program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/fshader.glsl"))
        close();

    // Link shader pipeline
    if (!program.link())
        close();

    // Bind shader pipeline for use
    if (!program.bind())
        close();
}
//! [3]

//! [4]
void MainWidget::initTextures()
{

    //----------------------------

    grassTexture = new QOpenGLTexture(QImage(":/grass.png").mirrored());
    // Set nearest filtering mode for texture minification
    grassTexture->setMinificationFilter(QOpenGLTexture::Nearest);

    // Set bilinear filtering mode for texture magnification
    grassTexture->setMagnificationFilter(QOpenGLTexture::Linear);

    // Wrap texture coordinates by repeating
    grassTexture->setWrapMode(QOpenGLTexture::Repeat);

    //----------------------------

    skyboxTexture[0] = new QOpenGLTexture(QImage(":/skyboxSide1.jpg").mirrored());
    // Set nearest filtering mode for texture minification
    skyboxTexture[0]->setMinificationFilter(QOpenGLTexture::Nearest);

    // Set bilinear filtering mode for texture magnification
    skyboxTexture[0]->setMagnificationFilter(QOpenGLTexture::Linear);

    // Wrap texture coordinates by repeating
    skyboxTexture[0]->setWrapMode(QOpenGLTexture::Repeat);

    //----------------------------

    skyboxTexture[1] = new QOpenGLTexture(QImage(":/skyboxSide2.jpg").mirrored());
    // Set nearest filtering mode for texture minification
    skyboxTexture[1]->setMinificationFilter(QOpenGLTexture::Nearest);

    // Set bilinear filtering mode for texture magnification
    skyboxTexture[1]->setMagnificationFilter(QOpenGLTexture::Linear);

    // Wrap texture coordinates by repeating
    skyboxTexture[1]->setWrapMode(QOpenGLTexture::Repeat);

    //----------------------------

    skyboxTexture[2] = new QOpenGLTexture(QImage(":/skyboxSide3.jpg").mirrored());
    // Set nearest filtering mode for texture minification
    skyboxTexture[2]->setMinificationFilter(QOpenGLTexture::Nearest);

    // Set bilinear filtering mode for texture magnification
    skyboxTexture[2]->setMagnificationFilter(QOpenGLTexture::Linear);

    // Wrap texture coordinates by repeating
    skyboxTexture[2]->setWrapMode(QOpenGLTexture::Repeat);

    //----------------------------

    skyboxTexture[3] = new QOpenGLTexture(QImage(":/skyboxSide4.jpg").mirrored());
    // Set nearest filtering mode for texture minification
    skyboxTexture[3]->setMinificationFilter(QOpenGLTexture::Nearest);

    // Set bilinear filtering mode for texture magnification
    skyboxTexture[3]->setMagnificationFilter(QOpenGLTexture::Linear);

    // Wrap texture coordinates by repeating
    skyboxTexture[3]->setWrapMode(QOpenGLTexture::Repeat);

    //----------------------------

    skyboxTexture[4] = new QOpenGLTexture(QImage(":/skyboxBottom.jpg").mirrored());
    // Set nearest filtering mode for texture minification
    skyboxTexture[4]->setMinificationFilter(QOpenGLTexture::Nearest);

    // Set bilinear filtering mode for texture magnification
    skyboxTexture[4]->setMagnificationFilter(QOpenGLTexture::Linear);

    // Wrap texture coordinates by repeating
    skyboxTexture[4]->setWrapMode(QOpenGLTexture::Repeat);

    //----------------------------

    skyboxTexture[5] = new QOpenGLTexture(QImage(":/skyboxTop.jpg").mirrored());
    // Set nearest filtering mode for texture minification
    skyboxTexture[5]->setMinificationFilter(QOpenGLTexture::Nearest);

    // Set bilinear filtering mode for texture magnification
    skyboxTexture[5]->setMagnificationFilter(QOpenGLTexture::Linear);

    // Wrap texture coordinates by repeating
    skyboxTexture[5]->setWrapMode(QOpenGLTexture::Repeat);

    //----------------------------



}
//! [4]

//! [5]
void MainWidget::resizeGL(int w, int h)
{
    windowSize[0] = w;
    windowSize[1] = h;

    scene->updateCameraWindowSize(windowSize);

    // Calculate aspect ratio
    qreal aspect = qreal(w) / qreal(h ? h : 1);

    // Set near plane to 3.0, far plane to 7.0, field of view 45 degrees
    const qreal zNear = 3.0, zFar = 7.0, fov = 45.0;
}
//! [5]

void MainWidget::paintGL()
{
    // Clear color and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(m_bCameraChange){
        Transform* cameraTransform = sceneCamera->getComponent<Transform>();
        Camera* cameraComponent = sceneCamera->getComponent<Camera>();

        //Lock translation on y axis
        cameraPos[1] = 0.0f;

        //Set camera position for debugging
        cameraTransform->translate(cameraPos);

        cameraComponent->setFOV(m_fCameraZoom);
        cameraComponent->setCameraFront(cameraFront);

        // Reset camera variables
        cameraPos = QVector3D();
        m_bCameraChange = false;
    }

    scene->update(&program, m_bDebugMode);
    scene->updatePhysics();
}
