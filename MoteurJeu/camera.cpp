#include "camera.h"

Camera::Camera() : Component("Camera"){
    setWindowSize(QVector2D(400.0f,400.0f));
}

Camera::Camera(Transform* in_linkedTransform, QVector2D in_windowSize) : Component("Camera"){
    setLinkedTransform(in_linkedTransform);
    setWindowSize(in_windowSize);
}

Camera::~Camera(){

}

QMatrix4x4 Camera::View(){
    QMatrix4x4 View = QMatrix4x4();
    QVector3D cameraPos = linkedTransform->getPosition();

    this->viewDir = linkedTransform->applyToVector(cameraFront).normalized();

    View.lookAt(
          cameraPos, // Camera is at the transform position
          cameraPos + viewDir, // and looks in front of itself
          UP  // Head is up (set to 0,-1,0 to look upside-down)
    );



    return View;
}
QMatrix4x4 Camera::Projection(){
    QMatrix4x4 Projection = QMatrix4x4();

    // Calculate aspect ratio
    qreal aspect = qreal(this->windowSize[0]) / qreal(this->windowSize[1] ? this->windowSize[1] : 1);

    // Reset projection
    Projection.setToIdentity();

    // Set perspective projection
    Projection.perspective(FOV, aspect, zNear, zFar);

    return Projection;
}

QMatrix4x4 Camera::Ortho(){
    QMatrix4x4 Ortho = QMatrix4x4();
    Ortho.ortho(0.0, this->windowSize[0], 0.0, this->windowSize[1], zNear, zFar);

    return Ortho;
}

QVector3D Camera::getViewDir(){
    return this->viewDir;
}

void Camera::setLinkedTransform(Transform* in_linkedTransform){
    this->linkedTransform = in_linkedTransform;
}

void Camera::setWindowSize(QVector2D in_windowSize){
    this->windowSize = in_windowSize;
}

void Camera::setFOV(float in_FOV){
    this->FOV = in_FOV;
}

void Camera::setCameraFront(QVector3D in_cameraFront){
    this->cameraFront = in_cameraFront;
}


