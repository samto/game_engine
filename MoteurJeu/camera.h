#ifndef CAMERA_H
#define CAMERA_H

#include "component.h"
#include "Transform.h"

constexpr QVector3D UP = QVector3D(0.0,1.0,0.0);
constexpr QVector3D RIGHT = QVector3D(1.0,0.0,0.0);
constexpr QVector3D FRONT = QVector3D(0.0,0.0,-1.0);
constexpr QVector3D WORLD_UP = QVector3D(0.0,1.0,0.0);

class Camera : public Component{
private:
    float zNear = 0.3f;
    float zFar = 1000.0f;
    float FOV = 45.0f;
    QVector3D cameraFront = FRONT;
    QVector3D viewDir;
    QVector2D windowSize;

    Transform* linkedTransform = nullptr;

public:
    Camera();
    Camera(Transform* in_linkedTransform, QVector2D in_windowSize);
    ~Camera();

    QMatrix4x4 View();
    QMatrix4x4 Projection();
    QMatrix4x4 Ortho();

    QVector3D getViewDir();

    void setLinkedTransform(Transform* in_linkedTransform);
    void setWindowSize(QVector2D in_windowSize);
    void setFOV(float in_FOV);
    void setCameraFront(QVector3D in_cameraFront);
};

#endif // CAMERA_H
