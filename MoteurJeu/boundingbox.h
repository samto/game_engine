#ifndef BOUNDINGBOX_H
#define BOUNDINGBOX_H

#include "component.h"
#include "mesh.h"
#include "Transform.h"

struct bbPoint;

class BoundingBox : public Component
{
private:
    Mesh* linkedMesh = nullptr;
    Transform* linkedTransform = nullptr;
    QVector3D startPoint = QVector3D();
    QVector3D endPoint = QVector3D();

    Mesh* debugBoundingBoxMesh = nullptr;
    std::vector<BoundingBox*> inCollisionWith = std::vector<BoundingBox*>();

    //Materials used to debug the collision boxes
    Material* green = new Material(QVector3D(0.0,1.0f,0.0f),QVector3D(0.0,0.0f,0.0f),QVector3D(0.0,0.0f,0.0f),0.0f);
    Material* red = new Material(QVector3D(1.0,0.0f,0.0f),QVector3D(0.0,0.0f,0.0f),QVector3D(0.0,0.0f,0.0f),0.0f);

public:
    BoundingBox();
    BoundingBox(Mesh* in_linkedMesh, Transform* in_linkedTransform);
    ~BoundingBox();

    void setLinkedComponents(Mesh* in_linkedMesh, Transform* in_linkedTransform);

    bbPoint getStartPoint();
    bbPoint getEndPoint();

    unsigned int getNbCollisions();
    void addCollision(BoundingBox* in_boundingBox);
    void removeCollision(BoundingBox* in_boundingBox);
    void clearCollisions();

    bool collidesWith(BoundingBox* in_boundingBox);
    BoundingBox* getCollisionByIndex(int index);

    void drawDebugBoundingBox(QOpenGLShaderProgram *program);
};

struct bbPoint{
    QVector3D position;
    BoundingBox* linkedBB;
    bbPoint(QVector3D in_position,BoundingBox* in_linkedBB):position(in_position),linkedBB(in_linkedBB){};
};

#endif // BOUNDINGBOX_H
