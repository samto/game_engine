#ifndef LIGHTING_H
#define LIGHTING_H

#include "component.h"
#include "Transform.h"

class Lighting : public Component{
private:
    QVector4D lightColor = QVector4D(1.0f,1.0f,1.0f,1.0f);

    Transform* linkedTransform = nullptr;

public:
    Lighting();
    Lighting(Transform* in_linkedTransform, QVector4D in_lightColor = QVector4D(1.0f,1.0f,1.0f,1.0f));
    ~Lighting();

    void setLightColor(QVector4D in_lightColor);
    QVector4D getLightColor();
    void setLinkedTransform(Transform* in_linkedTransform);
};

#endif // LIGHTING_H
