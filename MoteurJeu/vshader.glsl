#version 140
//La version 330 ne marche pas avec X2Go
//#version 330 core

uniform mat4 View;
uniform mat4 Projection;
uniform mat4 Model;
uniform vec3 lightPos;
uniform vec3 cameraPos;

in vec3 a_position;
in vec2 a_texcoord;
in vec3 a_normal;

out vec2 v_texcoord;
out vec3 v_position;
out vec3 v_normal;
out vec3 light_pos;
out vec3 camera_pos;

//! [0]
void main()
{
    // Pass texture coordinate to fragment shader
    // Value will be automatically interpolated to fragments inside polygon faces
    v_texcoord = a_texcoord;

    //Position of the vertex to give to the fragment shader
    v_position = vec3(Model * vec4(a_position,1.0));

    vec4 normal = inverse(transpose(Model)) * vec4(a_normal,0.0f);

    camera_pos = vec3(Model * vec4(cameraPos,1.0));

    v_normal = normalize(normal.xyz);

    light_pos = lightPos;

    // Calculate vertex position in screen space
    gl_Position = Projection * View * vec4(v_position,1.0f);
}
//! [0]
